### What is this repository for? ###

App created for better understending and learning how to using Hibernate framework, especially to build MySQL DataBases by annotations and to create relations between tables.
Also for practice in using DAO classes and creating tests.
There are still many things to do in this app, especially some front-end to give opportunity to use app methods by user.

### How do I get set up? ###
App is using standart tools Maven (POM), Hibarnete (hibernate.cfg.xml).
I was using MySQL Workbench 6.3 CE + server.