package medical.app.dao;

import medical.app.model.Speciality;
import org.hibernate.Session;

public class SpecialityDao extends AbstractDao<Speciality> {

	public SpecialityDao(Class<Speciality> t, Session session) {
		super(t, session);
	}

}
