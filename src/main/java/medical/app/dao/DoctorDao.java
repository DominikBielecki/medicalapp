package medical.app.dao;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import medical.app.model.Doctor;
import medical.app.model.Speciality;
import medical.app.model.SpecialityType;
import medical.app.model.Timetable;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import medical.app.exception.EntityNotFoundException;

public class DoctorDao extends AbstractDao<Doctor> {

	public DoctorDao(Class<Doctor> t, Session session) {
		super(t, session);
	}
	
	public List<Date> getAvailableAppointmentsById(Long id) throws EntityNotFoundException {
		
		Doctor doctor = getById(id);

		List<Date> appointmentDates = 
				doctor.getAppointments()
				.stream()
				.map(a -> a.getDate())
				.collect(Collectors.toList());
		
		List<Date> availableDates = new ArrayList<>();
		
		for(Timetable t : doctor.getTimetables()) {
			availableDates.addAll(compute(t.getStartJob(), t.getEndJob()));
		}
		
		availableDates.removeAll(appointmentDates);
		
		return availableDates;
	}
	
	private List<Date> compute(Date start, Date end) {
		List<Date> dates = new ArrayList<>();
		
		Instant instant = start.toInstant();
		LocalDateTime ldStart = instant
				.atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		instant = end.toInstant();
		LocalDateTime ldEnd = instant
				.atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		dates.add(Date.from(ldStart
				.atZone(ZoneId.systemDefault())
				.toInstant()
			)
		);
		
		while(ldStart.isBefore(ldEnd) 
				&& (ldStart.getHour() != ldEnd.getHour())) {
					
			ldStart = ldStart.plusMinutes(30);
			
			dates.add(Date.from(ldStart
					.atZone(ZoneId.systemDefault())
					.toInstant()
				)
			);
		}
		
		dates.remove(dates.size()-1);
		
		return dates;
	}
	
	public List<Doctor> getBySpeciality(SpecialityType type) {
		
		Criteria criteria = session.createCriteria(Speciality.class);
		criteria.add(Restrictions.eq("type", type));
		
		List<Speciality> specialities = criteria.list();
		
		List<Doctor> doctors = new ArrayList<>();
		
		for (Speciality s : specialities) {
			doctors.addAll(s.getDoctors());
		}
		
		return doctors;
	}
	
	
	public List<Doctor> getWorkerList(Date start, Date end) {
		
		Criteria crTimetable  = session.createCriteria(Timetable.class);
		
		crTimetable.add(Restrictions.le("startJob", start));
		crTimetable.add(Restrictions.ge("endJob", end));
		
		List<Timetable> times = (List<Timetable>) crTimetable.list();
		
		List<Doctor> doctors = new ArrayList<>();
		
		for (Timetable t : times) {
			doctors.addAll(t.getDoctors());
		}
		
		return doctors;
	}
	
	
	
	
	
	
	
	
	
//	public static void main(String[] args) {
//		
//		Date dateStart = new Date();
//		dateStart.setHours(10);
//		dateStart.setMinutes(0);
//		dateStart.setSeconds(0);
//		
//		Date dateEnd = new Date();
//		dateEnd.setHours(20);
//		dateEnd.setMinutes(0);
//		dateEnd.setSeconds(0);
//		
//		List<Date> dates = compute(dateStart, dateEnd);
//	
//		System.out.println(dates.size());
//		System.out.println(dates);
//	}
}
