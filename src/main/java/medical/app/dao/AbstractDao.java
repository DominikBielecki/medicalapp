package medical.app.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import medical.app.exception.EntityNotFoundException;

public abstract class AbstractDao<T> {

	private Class<T> genericObject;
	
	protected Session session;
	
	public AbstractDao(Class<T> t, Session session) {
		this.genericObject = t;
		this.session = session;
	}
	
	public List<T> getAll() {
		Criteria criteria = session.createCriteria(genericObject);
		List<T> result = criteria.list();
		
		return result;
	}
	
	public void save(T t) {
		session.save(t);
	}
	
	public void update(T t) {
		session.update(t);
	}
	
	public T getById(Long id) throws EntityNotFoundException {
		Criteria criteria = session
				.createCriteria(genericObject);
		
		criteria.add(Restrictions.eq("id", id));
		
		List<T> result = criteria.list();
		
		return result.stream()
				.findFirst()
				.orElseThrow(() -> 
				new EntityNotFoundException(
						String.format(
								"Entity with name %s and id %s not found", 
								genericObject, id)
						)
				);
	}
	
	public void delete(Long id) throws EntityNotFoundException {
		session.delete(getById(id));
	}
}
