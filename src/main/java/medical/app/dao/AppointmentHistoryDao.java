package medical.app.dao;

import medical.app.model.Appointment;
import medical.app.model.Patient;
import org.hibernate.Session;

import medical.app.model.Doctor;

public class AppointmentHistoryDao extends AbstractDao<Appointment> {
	
	private DoctorDao doctorDao = new DoctorDao(Doctor.class, session);
	private PatientDao patientDao = new PatientDao(Patient.class, session);

	public AppointmentHistoryDao(Class<Appointment> t, Session session) {
		super(t, session);
	}

}
