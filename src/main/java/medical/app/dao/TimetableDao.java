package medical.app.dao;

import medical.app.model.Timetable;
import org.hibernate.Session;

public class TimetableDao extends AbstractDao<Timetable> {

	public TimetableDao(Class<Timetable> t, Session session) {
		super(t, session);
	}

}
