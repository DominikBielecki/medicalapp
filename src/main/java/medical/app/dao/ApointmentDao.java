package medical.app.dao;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import medical.app.model.Patient;
import org.hibernate.Session;

import medical.app.exception.EntityNotFoundException;
import medical.app.model.AppointmentStatus;
import medical.app.model.Appointment;
import medical.app.model.Doctor;

public class ApointmentDao extends AbstractDao<Appointment> {

	private DoctorDao doctorDao = new DoctorDao(Doctor.class, session);
	private PatientDao patientDao = new PatientDao(Patient.class, session);

	public ApointmentDao(Class<Appointment> t, Session session) {
		super(t, session);
	}
	
	public void bookApointment(Long doctorId, String pesel, Date date) throws EntityNotFoundException {
		
		Doctor doctor = doctorDao.getById(doctorId);
		Patient patient = patientDao.getByPesel(pesel);
		
		Appointment appointment = new Appointment();
		appointment.setPatient(patient);
		appointment.setDoctor(doctor);
		appointment.setDate(date);
		
		save(appointment);
	}
	
	public void changeStatus(Long id, AppointmentStatus status) throws EntityNotFoundException {
		Appointment appointment = getById(id);
		appointment.setStatus(status);
		update(appointment);
	}
	
	public AppointmentStatus getStatus(Long id) throws EntityNotFoundException {
		return getById(id).getStatus();
	}
	
	public List<Appointment> getHistoricalAppointments(String pesel) throws EntityNotFoundException {
		return patientDao.getByPesel(pesel)
				.getAppointments()
				.stream()
				.filter(a -> !(a.getStatus().equals(AppointmentStatus.NEW)))
				.collect(Collectors.toList());
	}
	
	
	
	
	
	
	
	
	
	
	
}
