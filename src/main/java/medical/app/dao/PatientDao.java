package medical.app.dao;

import java.util.Date;
import java.util.List;

import medical.app.model.Appointment;
import medical.app.model.Patient;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import medical.app.exception.EntityNotFoundException;

public class PatientDao extends AbstractDao<Patient> {

	public PatientDao(Class<Patient> t, Session session) {
		super(t, session);
	}
	
	public List<Appointment> getPreviousAppointments(String pesel) throws EntityNotFoundException {
		return getAppointments(pesel, true);
	}
	
	public List<Appointment> getFutureAppointments(String pesel) throws EntityNotFoundException {
		return getAppointments(pesel, false);
	}
	
	public Patient getByPesel(String pesel) throws EntityNotFoundException {
		Criteria criteria = session.createCriteria(Patient.class);
		criteria.add(Restrictions.eq("pesel", pesel));
		
		List<Patient> patients = criteria.list();

		//zwraca pacjenta lub rzuca wyj�tek
		return patients.stream()
				.findFirst()
				.orElseThrow(
						() -> 
						new EntityNotFoundException(
								String.format("Entity with pesel %s no found", pesel)
						)
					);
	}
	
	private List<Appointment> getAppointments(String pesel, boolean previous) throws EntityNotFoundException {
		
		Criteria crPatient = session.createCriteria(Patient.class);
		//szukamy pacjenta z danym peselem
		crPatient.add(Restrictions.eq("pesel", pesel));
		
//		crPatient.setFetchMode("appointments", FetchMode.JOIN); 
		Criteria crAppointment = crPatient.createCriteria("appointments");
		//zaw�amy wynik do wizyt, kt�re by�y przed dat� obecn�
		
		if (previous) {
			crAppointment.add(Restrictions.lt("date", new Date())); //less than
		} else {
			crAppointment.add(Restrictions.gt("date", new Date())); //greater then
		}
		
		List<Patient> patients = crPatient.list();
		
		Patient p = patients.stream()
		.findFirst()
		.orElseThrow(
				() -> 
				new EntityNotFoundException(
						String.format("Entity with pesel %s no found", pesel)
				)
			);
		
		List<Appointment> apps = p.getAppointments();
		
		return apps;
	}
	
	public List<Patient> getByCity(String city) {
		
		Criteria crPatient = session.createCriteria(Patient.class);
		
		Criteria crAddress = crPatient.createCriteria("address");
		crAddress.add(Restrictions.eq("city", city));
		
		return (List<Patient>) crAddress.list();
	}
}
