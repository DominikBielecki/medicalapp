package medical.app.main;

import java.util.List;

import medical.app.model.Patient;
import org.hibernate.Session;
import org.hibernate.Transaction;

import medical.app.dao.DoctorDao;
import medical.app.dao.PatientDao;
import medical.app.exception.EntityNotFoundException;
import medical.app.model.Doctor;
import medical.app.model.SpecialityType;
import medical.app.util.HibernateUtil;

public class MainDao {

	public static void main(String[] args) throws EntityNotFoundException {
		
		Session session = HibernateUtil.getSession();
		Transaction tx = session.beginTransaction();
		
		PatientDao patientDao = new PatientDao(Patient.class, session);
		
//		List<Patient> patients = patientDao.getAll();
//		
//		patients
//		.stream()
//		.forEach(p -> System.out.println("name: " + p.getName()));
//		
//		Patient patient = patientDao.getById(1L);
//		System.out.println("surname by id: " + patient.getSurname());
//		
//		
//		patient.getAddress().setCity("Krakow");
//		
//		patient.setName("Krzysztof");
//		patientDao.update(patient);
//		patient = patientDao.getById(1L);
//		System.out.println("name by id: " + patient.getName());
		
//		patientDao.delete(1L);
		
		//---------------------------------------------
		
		final String pesel = "12345678901";
//		System.out.println(patientDao.getByPesel(pesel).getSurname());
	
//		System.out.println(patientDao.getPreviousAppointments(pesel).get(0).getDate());

		DoctorDao doctorDao = new DoctorDao(Doctor.class, session);
		
//		List<Date> dates = doctorDao.getAvailableAppointmentsById(1L);
//		System.out.println(dates.size());
//		System.out.println(dates);
		
		List<Doctor> doctors = doctorDao
				.getBySpeciality(SpecialityType.CARDIOLOGIST);


		for (Doctor doctor : doctors) {
			System.out.println(doctor.getSurname());
		}
		
		tx.commit();
		session.close();
	}
}
