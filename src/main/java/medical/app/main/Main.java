package medical.app.main;


import java.util.Arrays;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import medical.app.model.Address;
import medical.app.model.Appointment;
import medical.app.model.Doctor;
import medical.app.model.Patient;
import medical.app.model.Speciality;
import medical.app.model.SpecialityType;
import medical.app.model.Timetable;

public class Main {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = 
				new Configuration().configure().buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Patient patient = new Patient();
		patient.setName("Jan");
		patient.setSurname("Kowalski");
		patient.setPesel("12345678901");
		
		Address address = new Address();
		address.setCity("Warszawa");
		address.setStreet("Wawelska");
		
		patient.setAddress(address);
		address.setPatient(patient);
		
		
		Doctor doctor = new Doctor();
		doctor.setName("Marian");
		doctor.setSurname("Antek");
		
		Appointment ap = new Appointment();
		ap.setDate(new Date());
		ap.setDoctor(doctor);
		ap.setPatient(patient);
		
		doctor.setAppointments(Arrays.asList(ap));
		
		Speciality sp1 = new Speciality();
		sp1.setType(SpecialityType.CARDIOLOGIST);
		sp1.setDoctors(Arrays.asList(doctor));
		
		doctor.setSpecialities(Arrays.asList(sp1));
		
		Timetable t1 = new Timetable();
		t1.setStartJob(new Date());
		t1.setEndJob(new Date());
		t1.setDoctors(Arrays.asList(doctor));
		
		doctor.setTimetables(Arrays.asList(t1));
				
		session.save(patient);
		session.save(doctor);
		
		tx.commit();
		session.close();
		
		//-----------------------------------
		

		
		
	}

}
