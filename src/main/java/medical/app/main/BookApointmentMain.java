package medical.app.main;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import medical.app.dao.ApointmentDao;
import medical.app.model.Appointment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import medical.app.dao.DoctorDao;
import medical.app.exception.EntityNotFoundException;
import medical.app.model.Doctor;

public class BookApointmentMain {

	public static void main(String[] args) throws EntityNotFoundException {
		
		SessionFactory sessionFactory = 
				new Configuration().configure().buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Scanner sc = new Scanner(System.in);
		DoctorDao doctorDao = new DoctorDao(Doctor.class, session);
		ApointmentDao apointmentDao = new ApointmentDao(Appointment.class, session);
		
		System.out.println("Podaj pesel");
		String pesel = sc.next();
		
		System.out.println("Podaj id lekarza");
		Long doctorId = sc.nextLong();
		
		List<Date> dates = doctorDao.getAvailableAppointmentsById(doctorId);
		
		for (int i = 0; i < dates.size(); i++) {
			System.out.println(i+1 + ". " + dates.get(i));
		}
		
		System.out.println("Wybierz numer daty");
		int choise = sc.nextInt();
		
		apointmentDao.bookApointment(doctorId, pesel, dates.get(choise-1));
		
		sc.close();
		tx.commit();
		session.close();
	}

}
