package medical.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="speciality")
public class Speciality {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private SpecialityType type; 
	
	@ManyToMany(cascade=CascadeType.ALL, mappedBy="specialities")
	private List<Doctor> doctors = new ArrayList<>();
	
	public Speciality() {
		
	}

	public Speciality(SpecialityType type, List<Doctor> doctors) {
		this.type = type;
		this.doctors = doctors;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SpecialityType getType() {
		return type;
	}

	public void setType(SpecialityType type) {
		this.type = type;
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	@Override
	public String toString() {
		return "Speciality [id=" + id + ", type=" + type + ", doctors=" + doctors + "]";
	}
}
