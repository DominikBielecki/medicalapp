package medical.app.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "timetable")
public class Timetable {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;

	@Column(name = "start_job")
	private Date startJob;

	@Column(name = "end_job")
	private Date endJob;

	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "timetables")
	private List<Doctor> doctors = new ArrayList<>();

	public Timetable() {

	}

	public Timetable(Date startJob, Date endJob, List<Doctor> doctors) {
		this.startJob = startJob;
		this.endJob = endJob;
		this.doctors = doctors;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartJob() {
		return startJob;
	}

	public void setStartJob(Date startJob) {
		this.startJob = startJob;
	}

	public Date getEndJob() {
		return endJob;
	}

	public void setEndJob(Date endJob) {
		this.endJob = endJob;
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	@Override
	public String toString() {
		return "Timetable [id=" + id + ", startJob=" + startJob + ", endJob=" + endJob + ", doctors=" + doctors + "]";
	}
}
