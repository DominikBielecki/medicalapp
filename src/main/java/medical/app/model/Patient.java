package medical.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="patient")
public class Patient extends Person {
	
	@Column(length = 11, unique = true)
	private String pesel;
	
	@Column(name="insured")
	private Boolean isInsured;
	
	@OneToOne(mappedBy = "patient", cascade=CascadeType.ALL)
	private Address address;
	
	@OneToMany(mappedBy="patient", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Appointment> appointments = new ArrayList<>();
	
	public Patient() {
		
	}
	
	public Patient(String name, String surname, String pesel, Boolean isInsured, Address address,
			List<Appointment> appointments) {
		super();
		this.name = name;
		this.surname = surname;
		this.pesel = pesel;
		this.isInsured = isInsured;
		this.address = address;
		this.appointments = appointments;
	}

	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public Boolean getIsInsured() {
		return isInsured;
	}

	public void setIsInsured(Boolean isInsured) {
		this.isInsured = isInsured;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

//	@Override
//	public String toString() {
//		return "Patient [id=" + id + ", name=" + name + ", surname=" + surname + ", date=" + date + ", isInsured="
//				+ isInsured + ", address=" + address + "]";
//	}
}
