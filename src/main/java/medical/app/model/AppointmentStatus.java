package medical.app.model;

public enum AppointmentStatus {
	NEW, DONE, CANCELED, ABSENT
}
