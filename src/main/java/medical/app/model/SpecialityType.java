package medical.app.model;

public enum SpecialityType {

	GENERAL_INTERNIST, SURGEON, CARDIOLOGIST
	
}
