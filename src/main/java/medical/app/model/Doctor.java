package medical.app.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="doctor")
public class Doctor extends Person {
	
	@OneToMany(mappedBy="doctor", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<Appointment> appointments = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "doctor_speciality_join", 
		joinColumns = { @JoinColumn(name = "doctor_id") }, 
		inverseJoinColumns = {@JoinColumn(name = "speciality_id") }
	)
	private List<Speciality> specialities = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	
	@JoinTable(name = "doctor_timetable_join", 
		joinColumns = { @JoinColumn(name = "doctor_id") }, 
		inverseJoinColumns = {@JoinColumn(name = "timetable_id") })
	private List<Timetable> timetables = new ArrayList<>();
	
	public Doctor() {
		
	}

	public Doctor(String name, String surname, List<Appointment> appointments, List<Speciality> specialities,
			List<Timetable> timetables) {
		this.name = name;
		this.surname = surname;
		this.appointments = appointments;
		this.specialities = specialities;
		this.timetables = timetables;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	public List<Speciality> getSpecialities() {
		return specialities;
	}

	public void setSpecialities(List<Speciality> specialities) {
		this.specialities = specialities;
	}

	public List<Timetable> getTimetables() {
		return timetables;
	}

	public void setTimetables(List<Timetable> timetables) {
		this.timetables = timetables;
	}

//	@Override
//	public String toString() {
//		return "Doctor [id=" + id + ", name=" + name + ", surname=" + surname + ", appointments=" + appointments
//				+ ", specialities=" + specialities + ", timetables=" + timetables + "]";
//	}
}
