package medical.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="address")
public class Address {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;
	
	private String street;
	
	private Integer number;
	
	private String city;
	
	private String postalcode;
	
	@OneToOne
	@JoinColumn(name="patient_id")
	private Patient patient;
	
	public Address() {
		
	}
	
	public Address(String street, Integer number, String city, String postalcode, Patient patient) {
		this.street = street;
		this.number = number;
		this.city = city;
		this.postalcode = postalcode;
		this.patient = patient;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

//	@Override
//	public String toString() {
//		return "Address [id=" + id + ", street=" + street + ", number=" + number + ", city=" + city + ", postalcode="
//				+ postalcode + ", patient=" + patient + "]";
//	}
}
