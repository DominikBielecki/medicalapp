package medical.app.service;

import java.util.List;

import org.hibernate.Session;

import medical.app.dao.ApointmentDao;
import medical.app.exception.EntityNotFoundException;
import medical.app.model.Appointment;
import medical.app.model.AppointmentStatus;

public class AppoinmentService extends AbstractService<Appointment> {

	public AppoinmentService(Session session) {
		super(session);
	}

	@Override
	protected void initDao(Session session) {
		dao = new ApointmentDao(Appointment.class, session);		
	}
	
	public void changeStatus(Long id, AppointmentStatus status) throws EntityNotFoundException {
		AppointmentStatus actual = ((ApointmentDao) dao).getStatus(id);
		
		if (actual.equals(AppointmentStatus.NEW) && !actual.equals(status)) {
			((ApointmentDao) dao).changeStatus(id, status);
		} else {
			throw new IllegalArgumentException("Wrong status " + status);
		}
	}
	
	public List<Appointment> getHistoricalAppointments(String pesel) throws EntityNotFoundException {
		return ((ApointmentDao) dao).getHistoricalAppointments(pesel);
	}
}
