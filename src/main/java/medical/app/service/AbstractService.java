package medical.app.service;

import java.util.List;

import org.hibernate.Session;

import medical.app.dao.AbstractDao;
import medical.app.exception.EntityNotFoundException;

public abstract class AbstractService<T> {

	protected AbstractDao<T> dao;
	
	protected abstract void initDao(Session session);
	
	public AbstractService(Session session) {
		initDao(session);
	}
	
	public List<T> getAll() {
		return dao.getAll();
	}
	
	public T getById(Long id) throws EntityNotFoundException {
		return dao.getById(id);
	}
	
	public void save(T t) {
		dao.save(t);
	}
	
	public void update(T t) {
		dao.update(t);
	}
	
	public void delete(Long id) throws EntityNotFoundException {
		dao.delete(id);
	}
}
