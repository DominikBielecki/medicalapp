package medical.app.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private HibernateUtil() {
		
	}
	
	private static final SessionFactory sessionFactory = new Configuration()
			.configure().buildSessionFactory();
	
	public static Session getSession() {
		return sessionFactory.openSession();
	}
}
