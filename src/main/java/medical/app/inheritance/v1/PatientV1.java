package medical.app.inheritance.v1;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "patient_v1")
public class PatientV1 extends PersonV1 {

	private String pesel;

	public PatientV1() {
		
	}
	
	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
}
