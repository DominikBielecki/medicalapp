package medical.app.inheritance.v1;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="doctor_v1")
public class DoctorV1 extends PersonV1 {

	private String license;
	
	public DoctorV1() {
		
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
}
