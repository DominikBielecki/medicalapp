package medical.app.inheritance.v3;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) {

		SessionFactory sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		
		DoctorV3 d1 = new DoctorV3();
		d1.setName("Jan");
		d1.setSurname("Kowalski");
		d1.setLicense("123456");
		
		PatientV3 p1 = new PatientV3();
		p1.setName("Tomasz");
		p1.setSurname("Kot");
		p1.setPesel("12345678901");
		
		session.save(p1);
		session.save(d1);
		
		
		tx.commit();
		session.close();
		sessionFactory.close();
		
	}

}
