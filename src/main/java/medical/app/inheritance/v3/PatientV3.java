package medical.app.inheritance.v3;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "patient_v3")
@PrimaryKeyJoinColumn(name="id")
public class PatientV3 extends PersonV3 {

	private String pesel;

	public PatientV3() {
		
	}
	
	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
}
