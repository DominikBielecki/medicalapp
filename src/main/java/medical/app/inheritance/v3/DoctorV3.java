package medical.app.inheritance.v3;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="doctor_v3")
@PrimaryKeyJoinColumn(name="id")
public class DoctorV3 extends PersonV3 {

	private String license;
	
	public DoctorV3() {
		
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
}
