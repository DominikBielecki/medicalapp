package medical.app.inheritance.v2;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="doctor_v2")
@DiscriminatorValue(value = "DOCTOR")
public class DoctorV2 extends PersonV2 {

	private String license;
	
	public DoctorV2() {
		
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
}
