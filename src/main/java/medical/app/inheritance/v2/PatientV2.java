package medical.app.inheritance.v2;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "patient_v2")
@DiscriminatorValue(value = "PATIENT")
public class PatientV2 extends PersonV2 {

	private String pesel;

	public PatientV2() {
		
	}
	
	public String getPesel() {
		return pesel;
	}

	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
}
