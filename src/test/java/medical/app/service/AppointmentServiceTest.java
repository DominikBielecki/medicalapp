package medical.app.service;

import java.util.List;

import medical.app.dao.AbstractTest;
import org.junit.Assert;
import org.junit.Test;

import medical.app.exception.EntityNotFoundException;
import medical.app.model.Appointment;
import medical.app.model.AppointmentStatus;

public class AppointmentServiceTest extends AbstractTest {

	private AppoinmentService service;
	
	@Override
	protected void initDao() {
		
	}

	@Override
	protected void initService() {
		service = new AppoinmentService(session);
	}
	
	@Test
	public void shouldChangeAppointmentStatusToCanceled() throws EntityNotFoundException {
		final Long appointmentId = 1L;
		final AppointmentStatus givenStatus = AppointmentStatus.CANCELED;
		
		//when
		service.changeStatus(appointmentId, givenStatus);
		
		//then
		Assert.assertEquals(givenStatus, service.getById(appointmentId).getStatus());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowIllegalArgumentException() throws EntityNotFoundException {
		//given
		final Long appointmentId = 1L;
		Appointment a = service.getById(appointmentId);
		a.setStatus(AppointmentStatus.CANCELED);
		service.update(a);
		
		//when
		service.changeStatus(appointmentId, AppointmentStatus.NEW);
		
		//then throw exception
	}
	
	@Test
	public void shouldReturnEmptyHistoricalAppointmentList() throws EntityNotFoundException {
		//when
		List<Appointment> list = service
				.getHistoricalAppointments("12345678901");
		
		//then
		Assert.assertEquals(true, list.isEmpty());
	}
	
	@Test
	public void shouldReturn1HistoricalAppointmentWithStatusDone() throws EntityNotFoundException {
		//given
		final Long appointmentId = 1L;
		Appointment a = service.getById(appointmentId);
		a.setStatus(AppointmentStatus.DONE);
		service.update(a);
		
		//when
		List<Appointment> list = service
				.getHistoricalAppointments("12345678901");
		
		//then
		Assert.assertEquals(1, list.size());
		Assert.assertEquals(AppointmentStatus.DONE, list.get(0).getStatus());
	}
}
