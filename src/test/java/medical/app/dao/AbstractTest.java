package medical.app.dao;

import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;

import medical.app.model.Address;
import medical.app.model.Appointment;
import medical.app.model.Doctor;
import medical.app.model.Patient;
import medical.app.model.Speciality;
import medical.app.model.SpecialityType;
import medical.app.model.Timetable;

public abstract class AbstractTest {
	
	protected SessionFactory sessionFactory;
	protected Session session;
	protected Transaction tx;
	
	@Before
	public void setup() {
		sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		session = sessionFactory.openSession();
		tx = session.beginTransaction();
		setupDatabase();
		initDao();
		initService();
	}
	
	@After
	public void tearDown() {
		tx.commit();
		session.close();
		sessionFactory.close();
	}
	
	protected abstract void initDao();
	
	protected abstract void initService();
	
	private void setupDatabase() {
		Patient patient = new Patient();
		patient.setName("Jan");
		patient.setSurname("Kowalski");
		patient.setPesel("12345678901");
		
		Address address = new Address();
		address.setCity("Warszawa");
		address.setStreet("Wawelska");
		
		patient.setAddress(address);
		address.setPatient(patient);
		
		
		Doctor doctor = new Doctor();
		doctor.setName("Marian");
		doctor.setSurname("Antek");
		
		Appointment ap = new Appointment();
		
		Calendar cal = new GregorianCalendar(2017, 7, 10, 11, 0, 0);
		
		ap.setDate(cal.getTime());
		ap.setDoctor(doctor);
		ap.setPatient(patient);
		
		patient.setAppointments(Arrays.asList(ap));
		
		doctor.setAppointments(Arrays.asList(ap));
		
		Speciality sp1 = new Speciality();
		sp1.setType(SpecialityType.CARDIOLOGIST);
		sp1.setDoctors(Arrays.asList(doctor));
		
		doctor.setSpecialities(Arrays.asList(sp1));
		
		Timetable t1 = new Timetable();
		
		Calendar calStart = new GregorianCalendar(2017, 7, 10, 10, 0, 0);
		
		t1.setStartJob(calStart.getTime());
		
		Calendar calend = new GregorianCalendar(2017, 7, 10, 20, 0, 0);

		
		t1.setEndJob(calend.getTime());
		t1.setDoctors(Arrays.asList(doctor));
		
		doctor.setTimetables(Arrays.asList(t1));
				
		session.save(patient);
		session.save(doctor);
	}

}
