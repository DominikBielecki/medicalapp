package medical.app.dao;

import java.util.List;

import medical.app.model.Appointment;
import medical.app.model.Patient;
import org.junit.Assert;
import org.junit.Test;

import medical.app.exception.EntityNotFoundException;

public class PatientDaoTest extends AbstractTest {
	
	private PatientDao patientDao;
	
	@Test
	public void shouldGetAllPatients() {
		List<Patient> patients = patientDao.getAll();
		Assert.assertEquals(1, patients.size());
	}
	
	@Test
	public void shouldGetOneApointmentByGivenPesel() throws EntityNotFoundException {
		List<Appointment> dates = patientDao.getPreviousAppointments("12345678901");
		Assert.assertEquals(1, dates.size());
		Assert.assertEquals("12345678901", dates.get(0).getPatient().getPesel());
	}
	
	@Test
	public void shouldReturn1PatientByCity() {
		//when
		List<Patient> patients = patientDao.getByCity("Warszawa");
		
		//then
		Assert.assertEquals(1, patients.size());
	}
	
	@Test
	public void shouldReturnEmptyPatientListByCity() {
		//when
		List<Patient> patients = patientDao.getByCity("Krakow");
		
		//then
		Assert.assertTrue(patients.isEmpty());
	}

	@Override
	protected void initDao() {
		patientDao = new PatientDao(Patient.class, session);		
	}

	@Override
	protected void initService() {
		
	}
}
