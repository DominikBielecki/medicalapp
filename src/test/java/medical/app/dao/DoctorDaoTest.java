package medical.app.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import medical.app.model.Doctor;

public class DoctorDaoTest extends AbstractTest {

	private DoctorDao doctorDao;
	
	@Override
	protected void initDao() {
		doctorDao = new DoctorDao(Doctor.class, session);
	}

	@Override
	protected void initService() {

	}
	
	@Test
	public void shoudRetutn1Doctor() {
		//given
		Calendar calStart = new GregorianCalendar(2017, 7, 10, 15, 0, 0);
		Calendar calEnd = new GregorianCalendar(2017, 7, 10, 20, 0, 0);
		
		//when
		List<Doctor> docs = doctorDao.getWorkerList(calStart.getTime(), 
				calEnd.getTime());
		
		Assert.assertEquals(1, docs.size());
	}

}
